$(document).ready(function() {

    $.ajax({
        url: baseurl + "/samples",
        type: 'Get',
        async: false,
        success: function (data) {
            // alert(data.length)

            for (i = 0; i < data.length;i++){

                $('#listOfSamples').append(
                    '<tr>'+
                        '<td id='+data[i].uniqueSampleId+' onclick="extractId(this)">'+data[i].sampleId+'</td>'+
                        '<td id='+data[i].uniqueSampleId+' onclick="extractId(this)">'+data[i].sampleType+'</td>'+
                        '<td id='+data[i].uniqueSampleId+'>'+data[i].sampleStatus+
                        '<button id='+data[i].uniqueSampleId+' onclick="passIdToTests(this)" type="button" class="btn manageTest btn-link"><span>Manage </span></button>'+
                        '</td>'+
                    '</tr>');
            }
            
        },
        cache: false,
        contentType: false,
        
    });


    $("#addSamplesScreen").click(function(){
        location.href = 'addSample.html'
    });

    $("#sarathyHome").click(function(){
        location.href = '../sampleList.html'
    });

    $("#logout").click(function(){

        if (confirm('Are you sure you want to logout?')) {
            // Save it!
            $.ajax({
                url: baseurl + "/logout",
                type: 'POST',
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    location.href = 'index.html'
                }
            })
          } else {
            // Do nothing!
          }

    });
});

function extractId(row){
    // alert(row.id);
    window.localStorage.setItem("sampleId",row.id )
    window.location.href = 'sampleDetails.html';
}

function passIdToTests(row){
    // alert(row.id);
    window.localStorage.setItem("sampleId",row.id )
    window.location.href = 'sampleBasicInfo.html';
}