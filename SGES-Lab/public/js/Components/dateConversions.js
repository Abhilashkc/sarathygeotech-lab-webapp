
//Epoch to date conversions
function dateStringToCalendarFeedableDate(completeDateTime){
    console.log(completeDateTime.getUTCMonth())

    var dateString = ""
    var monthString = ""
    var yearString = ""
    var returnString = ""

    if(completeDateTime.getDate() < 10){
        dateString = "0"  + completeDateTime.getDate()
    }else{
        dateString = completeDateTime.getDate()
    }

    if(completeDateTime.getMonth() < 10){
        monthString = "0" + (completeDateTime.getMonth() + 1)
    } else{
        monthString = (completeDateTime.getMonth() + 1)
    }

    returnString = completeDateTime.getFullYear() + "-" + (monthString) + "-" + dateString
    console.log(returnString)
    return returnString

}

function convertEpochToDateTime(unixtimestamp){

   
    // Months array
    var months_arr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
   
    // Convert timestamp to milliseconds
    var date = new Date(unixtimestamp*1000);
   
    // Year
    var year = date.getFullYear();
   
    // Month
    var month = months_arr[date.getMonth()];
   
    // Day
    var day = date.getDate();
   
    // Hours
    var hours = date.getHours();
   
    // Minutes
    var minutes = "0" + date.getMinutes();
   
    // Seconds
    var seconds = "0" + date.getSeconds();
   
    // Display date time in MM-dd-yyyy h:m:s format
    var convdataTime = month+'-'+day+'-'+year+' '+hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    
    document.getElementById('datetime').innerHTML = convdataTime;
    
   }