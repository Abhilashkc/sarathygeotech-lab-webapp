var sampleId = window.localStorage.getItem("sampleId");

var selectedTest;
var testCardArray = [];

$(document).ready(function() {

    $.ajax({
        url: baseurl + "/samples/onesample/"+ sampleId,
        type: 'Get',
        success: function (data) {
            // console.log(data)
            $('#secondHeader').text("Sample - " + data[0].sampleId)

            if(data[0].sampleImageId != null){
                imageId = data[0].sampleImageId[0]
            }

            var arrivalCompleteDate = new Date(data[0].dateOfSampleArrival * 1000);
            var arrivalLoggingDate  = new Date(data[0].dateOfSampleLogging * 1000);
            

            // console.log(sampleArrivalDate)
            $('#sampleId').val(data[0].sampleId)
            $('#sampleType').val(data[0].sampleType)
            $('#sampleWeight').val(data[0].sampleWeight)
            $('#sampleStatus').val(data[0].sampleStatus)
            $('#dateOfSampleArrival').val(dateStringToCalendarFeedableDate(arrivalCompleteDate))
            $('#dateOfSampleLogging').val(dateStringToCalendarFeedableDate(arrivalLoggingDate))

        }
    });

    $.ajax({
        url: baseurl + "/tests",
        type: 'Get',
        success: function (data) {
            console.log(data)
            for(i = 0; i < data.length; i++){
                testCardArray.push(data[i].testCardNumber)

                    if(data[i].testStatus == "Planned"){
                        $('#list_of_test_planned').append(
                            '<tr id='+data[i].uniqueTestId+'>'+
                                '<td>'+testTypes[parseInt(data[i].testMethod)]+'</td>'+
                                '<td>'+data[i].testStatus+'</td>'+
                            '</tr>')
                    }
                    $('#list_of_complete_test').append(
                        '<tr id='+data[i].uniqueTestId+' onclick="testPage(this)">'+
                            '<td>'+(i+1)+'.</td>'+
                            '<td>'+testTypes[parseInt(data[i].testMethod)]+'</td>'+
                            '<td>'+data[i].testStatus+'</td>'+
                        '</tr>')

            }
        }
    });

    $("#addTestSubmit").click(function(){

        var data;
        data = new FormData();
        data.append( 'testMethod',parseInt($('#selectedTest').val() ));
        data.append('sampleId', sampleId)
        data.append('testStatus', "Planned")
        data.append('testCreationTime', parseInt(Date.now() / 1000) + 19800)

        $.ajax({
            url: baseurl + "/tests/plantest/",
            type: 'POST',
            data: data,
            async: false,
            success: function (response) {
                // alert("Data : " + JSON.stringify(response) )
                location.reload()
            },
            cache: false,
            contentType: false,
            processData: false
        })

    });

    $('#initTestCard').click(function(){

        if(testCardArray.includes($('#testCardVal').val())){
            alert("Test card number already present")
            return
        }

        // alert(JSON.stringify(selectedTest))
        var data;
        data = new FormData();
        data.append('geoTestId',  selectedTest.geoTestId[0]);
        data.append('sampleId',selectedTest.sampleId )
        data.append('testStatus','Initialized')
        data.append('testMethod',selectedTest.testMethod)
        data.append('testCardNumber',$('#testCardVal').val())
        data.append('testCreationTime', selectedTest.testCreationTime)

        $.ajax({
            url: baseurl + "/tests/updateTest/" + selectedTest.uniqueTestId,
            type: 'POST',
            data: data,
            async: false,
            success: function (response) {
                // alert(JSON.stringify(response))
                if(response.success == true){

                    if(selectedTest.testMethod == "0"){
                        window.localStorage.setItem("testObj",selectedTest.uniqueTestId);
                        window.location.href = 'moisture_content_test/moisture_content.html';
                    }else if(selectedTest.testMethod == "1"){
                        window.localStorage.setItem("testObj",selectedTest.uniqueTestId);
                        window.location.href = 'specific_gravity_test/specific_gravity.html';
                    }

                }else{
                    alert("test card generation card failed")
                    location.reload()
                }
                // window.location.href = 'specific_gravity_test/specific_gravity.html';

            },      
            cache: false,
            contentType: false,
            processData: false
        })
    })

    $("#sarathyHome").click(function(){
        location.href = 'sampleList.html'
    });

});

function testPage(row){

    $.ajax({
        url: baseurl + "/tests/onetest/" + row.id,
        type: 'Get',
        success: function (data) {
            console.log(data)
            selectedTest = data.testDetails[0]

            if(selectedTest.testStatus == "Planned"){

                $('#testCardModal').modal('show');

            }else if(selectedTest.testMethod == "0"){
                alert(row.id)
                window.localStorage.setItem("testObj",row.id);
                window.location.href = 'moisture_content_test/moisture_content.html';

            }
            else if(selectedTest.testMethod == "1"){

                window.localStorage.setItem("testObj",row.id);
                window.location.href = 'specific_gravity_test/specific_gravity.html';

            }
            
        }
    });
}