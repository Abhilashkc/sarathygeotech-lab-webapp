var testObject = window.localStorage.getItem("testObj");
// testObject = JSON.parse(testObject)

$(document).ready(function() {

var testInfo;

  $.ajax({
    url: baseurl + "/tests/onetest/" + testObject,
    type: 'Get',
    success: function (data) {
        console.log(data)
        testInfo = data.testDetails[0]
        $('#testStatus').val(testInfo.testStatus)
        $('#pageTextHeader').text("Test - " + testTypes[parseInt(testInfo.testMethod)])
        $('#labHumidity').val(testInfo.labHumidity)
        $('#labTemperature').val(testInfo.labTemperature)
        $('#testCardNumber').val(testInfo.testCardNumber)
        $('#testStartTime').val(testInfo.testStartTime)
        $('#testEndTime').val(testInfo.testEndTime)

        $('#testCardNumberOfTrials').val(testInfo.testCardNumber)
        $('#methodOfAdoptionOfTrials').val(testInfo.testMethod)
        $('#labTemperatureOfTrials').val(testInfo.labTemperature)

        $('#weightOfCupInresults').val(testInfo.weight_of_cup_W1)
        $('#weightOfCupWithWetSoilInResults').val(testInfo.weight_of_wet_soil_and_cup_W2)
        $('#weightOfCupWithDrySoilInResults').val(testInfo.weight_of_dry_soil_and_cup_W3)
        $('#waterContentInResults').val(testInfo.water_content_W)

        geoTestIdVal = testInfo.geoTestId[0]
        uniqueTestIdval = testInfo.uniqueTestId;
        testMethodString = testInfo.testMethod;
        testCreatedTime = testInfo.testCreatedTime;

    }
});

    $('#updateTrial').click(function(){

      var insertionEpoch = ((new Date($('#timeOfInsertionInOven').val())).getTime() + 19800) /1000;
      var removalEpoch = ((new Date($('#timeOfRemovalInOven').val())).getTime() + 19800) /1000;

      var updateTestObj = {
          "geoTestIdSelected" : geoTestIdVal,
          "methodOfTestAdopted" : $('#methodOfAdoptionOfTrials').val(),
          "timeOfInsertionInOven" : parseInt(insertionEpoch),
          "timeOfRemovalFromOven" : parseInt(removalEpoch),
          "cupNumber" : $('#cupNumber').val(),
          "weight_of_cup_W1" : parseFloat($('#weightOfEmptyCup').val()),
          "weight_of_wet_soil_and_cup_W2" : parseFloat($('#weightOfWetSoilAndCup').val()),
          "weight_of_dry_soil_and_cup_W3" : parseFloat($('#weightOfdrySoilAndCup').val()),
        }

        var updateGenericTestObj;
        updateGenericTestObj = new FormData();
        updateGenericTestObj.append('geoTestId',  testInfo.geoTestId);
        updateGenericTestObj.append('sampleId',testInfo.sampleId )
        updateGenericTestObj.append('testStatus','In Progress')
        updateGenericTestObj.append('testMethod',testInfo.testMethod)
        updateGenericTestObj.append('testCardNumber',testInfo.testCardNumber)
        updateGenericTestObj.append('labTemperature', parseFloat($('#labTemperatureOfTrials').val()))
        updateGenericTestObj.append('labHumidity', '')
        updateGenericTestObj.append('testCreationTime', testInfo.testCreationTime)

              $.ajax({
                url: baseurl + "/tests/updateTest/" + uniqueTestIdval,
                type: 'POST',
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false,
                  data: updateGenericTestObj,
                  success: function (data) {
                    alert("Success")
                    $.post(baseurl + "/moisturecontenttests/updatemoisturecontent/" + geoTestIdVal,updateTestObj, function(data, status){
                      alert("Updated");
                      location.reload()
                    });
                  }
              })
          
    })

    
    $("#sarathyHome").click(function(){
      location.href = '../sampleList.html'
    });


});