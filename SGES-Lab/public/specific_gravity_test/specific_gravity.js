var testObject = window.localStorage.getItem("testObj");
// testObject = JSON.parse(testObject)
// alert(testObject)

$(document).ready(function() {

var testInfo;

  $.ajax({
    url: baseurl + "/tests/onetest/" + testObject,
    type: 'Get',
    success: function (data) {
        console.log(data)
        testInfo = data.testDetails[0]
        $('#testStatus').val(testInfo.testStatus)
        $('#pageTextHeader').text("Test - " + testTypes[parseInt(testInfo.testMethod)])
        $('#labHumidity').val(testInfo.labHumidity)
        $('#labTemperature').val(testInfo.labTemperature)
        $('#testCardNumber').val(testInfo.testCardNumber)
        $('#testStartTime').val(testInfo.testStartTime)
        $('#testEndTime').val(testInfo.testEndTime)

        $('#testCardNumberOfTrials').val(testInfo.testCardNumber)
        $('#methodOfAdoptionOfTrials').val(testInfo.testMethod)
        $('#labTemperatureOfTrials').val(testInfo.labTemperature)

        $('#weightOfCupInresults').val(testInfo.weight_of_cup_W1)
        $('#weightOfCupWithWetSoilInResults').val(testInfo.weight_of_wet_soil_and_cup_W2)
        $('#weightOfCupWithDrySoilInResults').val(testInfo.weight_of_dry_soil_and_cup_W3)
        $('#waterContentInResults').val(testInfo.water_content_W)
    }
});


  $("#sarathyHome").click(function(){
      location.href = '../sampleList.html'
  });

  $('#cameraInput').on('change', function(e){
    $data = e.originalEvent.target.files[0];
      var reader = new FileReader();
     reader.onload = function(evt){
     $('#imageID').attr('src',evt.target.result);
     reader.readAsDataUrl($data);
   }});


});

function submit_trial()
{
    var myElement1 = document.getElementById("sample_id_specific_gravity").value;
    var myElement2 = document.getElementById("density_bottle_no_specific_gravity").value;
    var myElement3 = document.getElementById("temp_specific_gravity").value;
    var myElement4 = document.getElementById("K_specific_gravity").value;
    var myElement5 = document.getElementById("wt_density_bottle_specific_gravity").value;
    var myElement6 = document.getElementById("wt_density_bottle_n_drysolid_specific_gravity").value;
    var myElement7 = document.getElementById("wt_density_bottle_n_drysolid_n_water_specific_gravity").value;
    var myElement8 = document.getElementById("wt_density_bottle_n_water_specific_gravity").value;
        
    var data1 =
    {
        "Sample ID" : myElement1,
        "Density Bottle No." : myElement2,
        "Temperature (T°C)" : myElement3,
        "K" : myElement4,
        "Wt of density bottle (g) m1" : myElement5,
        "Wt of density bottle + dry soids (g) m2" : myElement6,
        "Wt of density bottle + dry soids + water (g) m3" : myElement7, 
        "Wt of density bottle + water (g) m4" : myElement8
    }
    
    console.log(data1);
}